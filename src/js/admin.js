import "@/styles/admin.scss";
import onLoad from "./onload.js";

onLoad(async function() {
  let page = 0;
  let pendingPage = 0;
  let reachedEnd = false;
  let reachedPendingEnd = false;
  const allUsers = document.getElementById("all-wrap");
  const pendingUsers = document.getElementById("pending-wrap");
  const pendingBody = document.getElementById("pending-body");
  const allBody = document.getElementById("all-body");
  const allLoaded = {};
  const elements = {};
  const clones = {};
  const clickHooks = {};

  const pendingElements = {}; // This has only a fraction of them, just for manipulation when the thing is clicked
  async function renderUsers() {
    const users = await client.adminUsers(page++);
    if (users.length < 20) reachedEnd = true;
    ingestAll(users);
  }
  async function renderPending() {
    const users = await client.adminPending(pendingPage++);
    if (users.length < 20) reachedPendingEnd = true;
    ingestAll(users);
  }
  function ingestAll(users) {
    for (const user of users) {
      if (allLoaded[user.user_id]) continue; // No need to dupe it!
      allLoaded[user.user_id] = user;
      const userWrap = renderUser(user);
      fragmentAll.appendChild(userWrap);
      elements[user.user_id] = userWrap;
      if (!user.active) {
        const clone = userWrap.cloneNode(true);
        clone
          .getElementsByClassName("enable-btn")[0] // I'm not happy about this.
          .addEventListener("click", clickHooks[user.user_id]);
        clones[user.user_id] = clone;
        fragmentPending.appendChild(clone);
      }
    }
    allBody.appendChild(fragmentAll);
    pendingBody.appendChild(fragmentPending);
    fragmentAll = document.createDocumentFragment();
    fragmentPending = document.createDocumentFragment();
  }
  const { domains } = await client.getDomains();
  await renderUsers().then(() => reachedEnd || renderPending());

  function usernameClick(user) {
    modalUsername.innerText = user.username;
    modal.classList = `modal fade ${user.admin ? "user-admin" : ""}`;
  }

  function renderUser(user) {
    const wrap = document.createElement("tr");
    wrap.classList = "user-card";

    const username = document.createElement("td");
    const usernameWrap = document.createElement("a");
    usernameWrap.src = `#user-${user.user_id}`; // We grab from this when in modal
    usernameWrap.innerText = user.username;
    usernameWrap.setAttribute("data-toggle", "modal");
    usernameWrap.setAttribute("data-target", "#user-modal");
    // This is potentially an eval, sorry. If you've got any better ideas let me know
    usernameWrap.setAttribute(
      "onclick",
      `(${usernameClick.toString()})(${JSON.stringify(user)})`
    );
    username.appendChild(usernameWrap);
    if (user.admin) {
      const badge = document.createElement("span");
      badge.classList = "badge badge-pill badge-primary";
      badge.innerText = "ADMIN";
      username.appendChild(badge);
    }
    const email = document.createElement("td");
    email.innerText = user.email;
    const paranoid = document.createElement("td");
    paranoid.innerText = !!user.paranoid ? "Yes" : "No";
    const gdpr = document.createElement("td");
    gdpr.innerText =
      user.consented === null ? "None" : user.consented ? "All" : "Limit";
    const domain = document.createElement("td");
    domain.innerText = domains[String(user.domain)];
    const enableWrap = document.createElement("td");
    const enable = document.createElement("button");
    enable.innerText = user.active ? "Deactivate" : "Activate";
    enable.classList = `btn btn-${
      user.active ? "danger" : "success"
    } enable-btn`;
    // We check the token just in case we're not ready with profile request yet
    if (user.user_id == client.token.split(".")[0]) enable.disabled = true;
    enableWrap.appendChild(enable);

    clickHooks[user.user_id] = async function() {
      if (user.active) {
        await client.adminDeactivate(user.user_id);
      } else {
        await client.adminActivate(user.user_id);
      }
      user.active = !user.active;
      enable.innerText = user.active ? "Deactivate" : "Activate";
      enable.classList = `btn btn-${
        user.active ? "danger" : "success"
      } enable-btn`;
      if (!clones[user.user_id] && !user.active) {
        clones[user.user_id] = wrap.cloneNode(true);
        clones[user.user_id]
          .getElementsByClassName("enable-btn")[0]
          .addEventListener("click", clickHooks[user.user_id]);

        pendingBody.appendChild(clones[user.user_id]);
      }
      if (clones[user.user_id] && !user.active) {
        clones[user.user_id].style.display = "table-row";
      } else if (user.active && clones[user.user_id]) {
        clones[user.user_id].style.display = "none";
      }
    };
    enable.addEventListener("click", clickHooks[user.user_id]);
    const id = document.createElement("td");
    id.innerText = user.user_id;
    const order = [username, id, email, domain, paranoid, gdpr, enableWrap];
    for (const field of order) {
      wrap.appendChild(field);
    }
    return wrap;
  }

  let scrollingLocked = false;

  pendingUsers.addEventListener("scroll", async ev => {
    if (scrollingLocked) return; // Don't load all at once
    if (
      pendingUsers.offsetHeight + pendingUsers.scrollTop >=
        pendingUsers.scrollHeight &&
      !reachedEnd &&
      !reachedPendingEnd
      // We check both because our ingest does it automagically!
    ) {
      // got the bottom of the thing, load some more
      scrollingLocked = true;
      // Make sure we never stay locked. Never *ever*
      try {
        await renderUsers();
      } catch (err) {}
      scrollingLocked = false;
    }
  });

  allUsers.addEventListener("scroll", async ev => {
    if (scrollingLocked) return; // Don't load all at once
    if (
      allUsers.offsetHeight + allUsers.scrollTop >= allUsers.scrollHeight &&
      !reachedEnd
    ) {
      console.log("uwo");
      // got the bottom of the thing, load some more
      scrollingLocked = true;
      try {
        await renderUsers();
      } catch (err) {}
      scrollingLocked = false;
    }
  });

  const searchKeys = {
    email: user => user.email,
    domain: user => domains[user.domain],
    username: user => user.username,
    id: user => user.user_id
  };

  const search = document.getElementById("search");
  const limits = document.getElementById("search-limits");
  search.addEventListener("keyup", function(ev) {
    const val = search.value;
    const keys =
      limits.value == "*" ? Object.keys(searchKeys) : limits.value.split(",");
    for (const userId in allLoaded) {
      const user = allLoaded[userId];
      let matches = false;
      for (const key of keys) {
        const transformed = searchKeys[key](user);
        if (String(transformed).includes(val)) {
          matches = true;
        }
      }
      if (matches) elements[user.user_id].style.display = "table-row";
      else elements[user.user_id].style.display = "none";
    }
  });
});

// Create fragment after hooking our events, just in-case!
let fragmentAll = document.createDocumentFragment();
let fragmentPending = document.createDocumentFragment();
