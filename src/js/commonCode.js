import uuid from "uuid/v1";

function sendAlert(type, message) {
  const id = uuid();
  const alertElem = document.createElement("div");
  alertElem.classList = `alert alert-${type}`;
  alertElem.attributes.role = "alert";
  alertElem.innerText = message;
  alertElem.id = id;
  document.getElementById("alert-area").appendChild(alertElem);
  return id;
}

function removeAlert(id) {
  const elem = document.getElementById(id);
  if (elem) elem.remove();
}

let alertBox = null;

function handleErr(err) {
  if (alertBox) removeAlert(alertBox);
  if (err) {
    if (err.userMessage) {
      alertBox = sendAlert("danger", err.userMessage);
    } else {
      alertBox = sendAlert(
        "danger",
        `An unknown error occurred: ${err.message}`
      );
      throw err;
    }
  }
}

export default {
  handleErr,
  client: window.client,
  sendAlert,
  removeAlert
};
