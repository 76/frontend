import "@/styles/account.scss";
import "@/styles/forms.scss";
import onLoad from "./onload.js";
import common from "./commonCode.js";
import filesize from "file-size";

const EMAIL_RE = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const quotaPromise = client.getQuota();
const statPromise = client.getStats();
const dumpPromise = client.dumpStatus();
const domainPromise = client.getDomains();

// Make sure we tell superagent it's good to go
[quotaPromise, statPromise, dumpPromise, domainPromise].forEach(p =>
  p.then(r => r)
);

onLoad(async function() {
  // TODO: lazy load the rest of this so we can work with responses as we get them (just domains and profile left to figure out since they're tricky)
  let errorBox = null;
  const domainSelector = document.getElementById("domain-selector");
  const shortenDomainSelector = document.getElementById("s-domain-selector");
  const wildcard = document.getElementById("wildcard");
  const email = document.getElementById("email");
  const username = document.getElementById("profile-username");
  const paranoid = document.getElementById("paranoid");
  const tokenPassword = document.getElementById("token-password");
  const generateTokenBtn = document.getElementById("generate-token");
  const deleteForm = document.getElementById("delete-form");
  const deletePassword = document.getElementById("delete-password");
  const deleteBtn = document.getElementById("delete-btn");
  const deleteAccountBtn = document.getElementById("delete-modal-btn");
  const passwordForm = document.getElementById("password-form");
  const userId = document.getElementById("user-id");
  const revokePassword = document.getElementById("revoke-password");
  const revokeBtn = document.getElementById("revoke-btn");
  const stayLoggedIn = document.getElementById("stay-logged-in");
  const revokeForm = document.getElementById("revoke-form");
  const updateForm = document.getElementById("update-form");
  const submitBtn = document.getElementById("submit-btn");
  const newPassword = document.getElementById("new-password1");
  const password = document.getElementById("password");
  const newPassword2 = document.getElementById("new-password2");
  const totalShortens = document.getElementById("total-shortens");
  const totalDeleted = document.getElementById("total-deleted");
  const totalUsage = document.getElementById("total-usage");
  const totalFiles = document.getElementById("total-files");
  const profileQuota = document.getElementById("profile-quota");
  const profileUsed = document.getElementById("profile-used");
  const shortenProfileUsed = document.getElementById("s-profile-used");
  const shortenProfileQuota = document.getElementById("s-profile-quota");
  const shortenWildcard = document.getElementById("s-wildcard");
  const queuePosition = document.getElementById("queue-position");
  const dumpWrap = document.getElementById("datadump");
  const fileNum = document.getElementById("file-num");
  const fileMax = document.getElementById("file-max");
  const dumpBtn = document.getElementById("request-data");

  async function renderDataDump(dumpStatus = null) {
    if (!dumpStatus) dumpStatus = await client.dumpStatus();
    if (dumpStatus.state == "not_in_queue") {
      dumpWrap.classList = "state-none";
    } else if (dumpStatus.state == "in_queue") {
      dumpWrap.classList = "state-requested";
      queuePosition.innerText = ordinal(dumpStatus.position);
    } else if (dumpStatus.state == "processing") {
      dumpWrap.classList = "state-processing";
      fileNum.innerText = dumpStatus.files_done;
      fileMax.innerText = dumpStatus.total_files;
    }
  }

  dumpPromise.then(dump => renderDataDump(dump));

  window.profilePromise.then(profile => {
    userId.innerText = `${client.token.split(".")[0]}`;
    username.innerText = profile.username;
    if (profile.admin) {
      const adminBadge = document.createElement("span");
      adminBadge.innerText = "ADMIN";
      adminBadge.classList = "badge badge-pill badge-primary admin-badge";
      username.parentNode.appendChild(adminBadge);
    }
  });

  quotaPromise.then(quota => {
    profileQuota.innerText = filesize(quota.limit).human(); // / 1024 / 1024;
    profileUsed.innerText = filesize(quota.used).human(); // Math.round(
    //   quota.used / 1024 / 1024 || 0
    // );
    shortenProfileUsed.innerText = quota.shortenused || 0;
    shortenProfileQuota.innerText = quota.shortenlimit;
  });

  statPromise.then(stats => {
    totalShortens.innerText = stats.total_shortens;
    totalFiles.innerText = stats.total_files;
    totalDeleted.innerText = stats.total_deleted_files;
    totalUsage.innerText = filesize(stats.total_bytes).human();
  });

  tokenPassword.addEventListener("keydown", function(ev) {
    if (ev.key == "Enter") {
      createToken();
    }
  });
  updateForm.addEventListener("submit", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    return false;
  });

  generateTokenBtn.addEventListener("click", function() {
    createToken();
  });

  dumpBtn.addEventListener("click", async function() {
    await client.requestDump();
    await renderDataDump();
  });

  async function createToken() {
    try {
      const token = await client.generateToken(tokenPassword.value);
      window.location.hash = token;
      window.location.pathname = "/token.html";
    } catch (err) {
      tokenPassword.setCustomValidity(err.userMessage || err.message);
      passwordForm.classList = "was-validated needs-validation";
      tokenPassword.value = "";
      tokenPassword.focus();
      return;
    }
  }
  passwordForm.addEventListener("submit", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    return false;
  });

  revokeBtn.addEventListener("click", function() {
    revokeToken();
  });
  revokePassword.addEventListener("keydown", function(ev) {
    if (ev.key == "Enter") {
      revokeToken();
    }
  });

  deleteForm.addEventListener("submit", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    return false;
  });

  deleteBtn.addEventListener("click", function() {
    deleteAccount();
  });
  deletePassword.addEventListener("keydown", function(ev) {
    if (ev.key == "Enter") {
      deleteAccount();
    }
  });
  async function deleteAccount() {
    try {
      const emailToken = await client.deleteAccount(deletePassword.value);
      deleteAccountBtn.click();
    } catch (err) {
      deletePassword.setCustomValidity(err.userMessage || err.message);
      deleteForm.classList = "needs-validation was-validated";
    }
  }

  async function revokeToken() {
    try {
      await client.revokeTokens(revokePassword.value);
      if (stayLoggedIn.checked) {
        const token = await client.login(
          client.profile.username,
          revokePassword.value
        );
        window.localStorage.setItem("token", token);
        // Reload
        window.location.href = "";
        return;
      }
      window.location.pathname = "/";
    } catch (err) {
      revokePassword.setCustomValidity(err.userMessage || err.message);
      revokeForm.classList = "was-validated needs-validation";
      revokePassword.value = "";
      revokePassword.focus();
      return;
    }
  }

  await window.profilePromise;
  const { domains, officialDomains } = await domainPromise;
  console.log("Fetched domains:", domains);
  const options = Object.entries(domains).map(
    ([id, domain]) => new Option(domain.replace("*.", ""), id, false, false)
  );

  for (const option of options) {
    domainSelector.appendChild(option);
    shortenDomainSelector.appendChild(option.cloneNode(true));
  }
  domainSelector.value = window.client.profile.domain;
  shortenDomainSelector.value =
    client.profile.shorten_domain || client.profile.domain;
  wildcard.value = window.client.profile.subdomain || "";
  shortenWildcard.value =
    (client.profile.shorten_domain
      ? client.profile.shorten_subdomain
      : client.profile.subdomain) || "";

  email.value = window.client.profile.email || "";

  function checkShortenDomainSelector() {
    if (domains[shortenDomainSelector.value].startsWith("*.")) {
      shortenDomainSelector.parentNode.classList = "input-group show-wildcard";
    } else {
      shortenDomainSelector.parentNode.classList = "input-group";
    }
  }

  function checkDomainSelector() {
    if (domains[domainSelector.value].startsWith("*.")) {
      domainSelector.parentNode.classList = "input-group show-wildcard";
    } else {
      domainSelector.parentNode.classList = "input-group";
    }
  }
  try {
    checkDomainSelector();
    checkShortenDomainSelector();
  } catch (err) {}
  // This would be an addEventListener but jQuery is garbage
  domainSelector.onchange = checkDomainSelector;
  shortenDomainSelector.onchange = checkShortenDomainSelector;

  let domainId = client.profile.domain;
  let wildcardVal = client.profile.subdomain;
  let shortenDomainId = isNaN(client.profile.shorten_domain)
    ? domainId
    : client.profile.shorten_domain;
  let shortenWildcardVal = client.profile.subdomain;
  let emailVal = client.profile.email;
  let paranoidVal = client.profile.paranoid;
  paranoid.value = paranoidVal.toString();
  submitBtn.addEventListener("click", async function() {
    common.handleErr();
    common.removeAlert(errorBox);
    let error = false;
    if (newPassword.value && newPassword2.value != newPassword.value) {
      newPassword2.setCustomValidity("Doesn't match!");
      error = true;
    }
    if (!password.value) {
      password.setCustomValidity("Invalid password!");
      error = true;
    }
    if (!email.value.match(EMAIL_RE)) {
      email.setCustomValidity("Invalid email!");
      error = true;
    }
    updateForm.classList = "was-validated needs-validation form-wrap";
    if (error) return;

    const modifications = {};

    if (newPassword.value) modifications.new_password = newPassword.value;
    if (shortenDomainSelector.value != shortenDomainId) {
      modifications.shorten_domain = Number(shortenDomainSelector.value);
    }

    if (
      shortenWildcard.value != shortenWildcardVal &&
      domains[shortenDomainSelector.value].startsWith("*.")
    )
      modifications.shorten_subdomain = shortenWildcard.value;

    if (domainSelector.value != domainId) {
      modifications.domain = Number(domainSelector.value);
    }
    if (
      wildcard.value != wildcardVal &&
      domains[domainSelector.value].startsWith("*.")
    )
      modifications.subdomain = wildcard.value;

    if (paranoid.value != paranoidVal) {
      modifications.paranoid = paranoid.value == "true";
    }

    if (!Object.keys(modifications).length) return; // No changes to be made
    modifications.password = password.value;

    if (email.value != emailVal) {
      modifications.email = email.value;
    }

    try {
      await client.updateAccount(modifications);
      errorBox = common.sendAlert("success", "Your changes have been saved!");
      if (modifications.domain) domainId = modifications.domain;
      if (modifications.subdomain) wildcardVal = modifications.subdomain;
      if (modifications.shorten_domain) shortenDomainId = modifications.domain;
      if (modifications.shorten_subdomain)
        shortenWildcardVal = modifications.subdomain;
      if (modifications.email) emailVal = modifications.email;
      if (modifications.paranoid !== undefined) paranoidVal = paranoid.val;
    } catch (err) {
      if (err.errorCode == "BAD_AUTH") {
        password.setCustomValidity(err.userMessage);
        updateForm.classList = "was-validated needs-validation form-wrap";
      } else {
        common.handleErr(err);
      }
      return;
    }
    if (modifications.new_password) {
      window.localStorage.setItem("token", client.token);
      window.location.href = "";
    }
  });
});
