import "./commonCode.js";
import onLoad from "./onload.js";

const authenticatedPages = [
  "/upload.html",
  "/logout.html",
  "/account.html",
  "/list.html",
  "/shortlist.html",
  "/shorten.html",
  "/admin2.html"
];

const adminPages = ["/admin2.html"];

onLoad(async function() {
  console.log("Dom loaded");
  const acc = document.getElementById("nav-account");
  const navLinks = document.getElementsByClassName("intellectual-link");
  for (const navLink of navLinks) {
    const targetURL = navLink.attributes["data-target"].value;
    if (window.location.pathname == targetURL) {
      if (navLink.classList.contains("nav-link"))
        navLink.parentNode.classList += " active";
      continue;
    }
    navLink.href = targetURL;
  }

  const profileDisplay = document.getElementById("profile-container");
  const profileCache = JSON.parse(
    window.localStorage.getItem("profile-cache") || null
  );
  if (profileCache) isLoggedIn(profileCache);
  window.client.profile = await profilePromise;
  console.log(window.client.profile);
  function isLoggedIn(profile) {
    acc.innerText = profile.username;
    document.body.classList += " logged-in";
    if (profile.admin) {
      document.body.classList += " is-admin";
    }
  }
  if (!window.client.profile) {
    window.localStorage.removeItem("profile-cache");
    acc.innerText = "Account";
    document.body.classList = document.body.classList
      .toString()
      .replace("logged-in", "")
      .replace("is-admin", "");
  }
  if (window.client.profile) {
    isLoggedIn(window.client.profile);
    window.localStorage.setItem(
      "profile-cache",
      JSON.stringify(window.client.profile)
    );
  } else if (authenticatedPages.includes(window.location.pathname)) {
    // Hash param is used to know where to redirect back to after login.
    document.getElementById("garfield-login").href = `/login.html#${
      window.location.pathname
    }`;
    document.body.classList += " show-garfield";
    document.title = "Access Denied | Elixire";
  }

  if (
    window.client.profile &&
    !window.client.profile.admin &&
    adminPages.includes(window.location.pathname)
  ) {
    document.getElementById("garfield-login").href = `/login.html#${
      window.location.pathname
    }`;
    document.body.classList += " show-garfield";
    document.title = "Access Denied | Elixire";
  }

  const gdprFuckJquery = document.getElementById("open-gdpr-modal");
  if (
    window.client.profile &&
    (window.client.profile.consented === null ||
      window.location.hash == "#ANNOY_ME_TO_DEATH")
  ) {
    gdprFuckJquery.click();
  }
  const acceptBtn = document.getElementById("gdpr-btn");
  // const denyBtn = document.getElementById("gdpr-deny");
  const deleteBtn = document.getElementById("gdpr-delete");
  const password = document.getElementById("gdpr-password");
  const form = document.getElementById("gdpr-form");
  const dismissBtn = document.getElementById("dismiss-modal");
  const dataProcessing = document.getElementById("gdpr-minimal");
  password.addEventListener("keypress", function(ev) {
    if (ev.key == "Enter") {
      ev.preventDefault();
      ev.stopPropagation();
      submitGdpr(true);
      return false;
    }
  });
  acceptBtn.addEventListener("click", () => submitGdpr(dataProcessing.checked));

  deleteBtn.addEventListener("click", async function() {
    try {
      // if (password.value.length < 8 || password.value.length > 100) {
      //   throw { userMessage: "Invalid password" };
      // }
      await client.deleteAccount(password.value);
      alert(
        "Sorry to see you go, check your email for a verification link... :("
      );
    } catch (err) {
      password.setCustomValidity(err.userMessage || err.message);
      form.classList = "needs-validation was-validated logged-in-only";
    }
  });

  async function submitGdpr(allowed) {
    if (window.client.profile) {
      try {
        // if (password.value.length < 8 || password.value.length > 100) {
        //   throw new Error("BAD_AUTH");
        // }
        await client.updateAccount({
          password: password.value,
          consented: allowed
        });
      } catch (err) {
        form.classList = "was-validated needs-validation logged-in-only";
        return password.setCustomValidity(err.userMessage || err.message);
      }
    }
    window.localStorage.setItem("gdpr-consent", allowed);
    gdprFuckJquery.click();
  }
});
window.addEventListener("error", function(event) {
  // oopsie woopsie
  console.log(event);
  document.body.classList += " error-thrown";
  const errorInfo = document.getElementById("error-info");
  errorInfo.innerText = event.message;
  const filename = document.getElementById("filename");
  filename.innerText = `${event.filename}:${event.lineno}:${event.colno}`;
});
